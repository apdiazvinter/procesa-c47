# -*- coding: utf-8 -*-
import sys
from PyQt5.QtCore import pyqtSlot, QDate
from PyQt5.QtWidgets import QApplication, QDialog
from PyQt5.uic import loadUi
import psycopg2
import aux_functions as aux


class Ventana(QDialog):
    """
    Esta es la clase que despliega el widget que se utiliza para:

    - seleccionar la fecha del archivo y
    - ejecutar el proceso completo.

    """
    def __init__(self):
        super(Ventana, self).__init__()
        loadUi('carga_c47_y_csv.ui', self)
        self.setWindowTitle("Carga C47")
        self.dateEdit.setCalendarPopup(True)
        self.dateEdit.setDate(QDate.currentDate())
        self.pushButton.clicked.connect(self.proceso)
        self.label.setText('Fecha de Proceso')
        self.registro_1 = []
        self.registro_2 = []
        self.registro_3 = []

    @pyqtSlot()
    def proceso(self):
        """
        Lee, formatea como csv e inserta en BBDD los registros de tipo 1, 2 y 3 del archivo C47.
        El csv generado se graba en el mismo directorio donde reside este programa.

        :return:
        """
        fecha = self.dateEdit.date()
        dia = aux.pad_integer(fecha.day())
        mes = aux.pad_integer(fecha.month())
        agno = aux.pad_integer(fecha.year())
        fecha_proceso = agno + '-' + mes + '-' + dia

        # Registro 1
        self.get_registro_1(dia, mes, agno)
        self.insert_registro_1(fecha_proceso)
        print "C47 registro 1 Ok"

        # Registro 2
        self.get_registro_2(dia, mes, agno)
        self.insert_registro_2(fecha_proceso)
        print "C47 registro 2 Ok"

        # Registro 3
        self.get_registro_3(dia, mes, agno)
        self.insert_registro_3(fecha_proceso)
        print "C47 registro 3 Ok"

        return

    def insert_registro_1(self, fecha_proceso):
        """
        Inserta en BBDD los contenidos del registro 1 del archivo C47 a una fecha de proceso.
        Los contenidos deben estar almacenados en self.registro_1.

        :param fecha_proceso: la fecha de proceso del archivo.
        :return:
        """
        conn_string = "dbname=balance user=postgres password=Binter2015 host=172.16.50.40 port =5432"
        with psycopg2.connect(conn_string) as conn:
            cursor = conn.cursor()
            qry = "DELETE FROM public.c47_registro_1 WHERE fecha = '" + fecha_proceso + "'"
            cursor.execute(qry)
            qry = "INSERT INTO public.c47_registro_1 (registro, fecha, consolidacion, contraparte, "
            qry += "concentracion_mayorista, renovacion_mayorista) VALUES "
            for linea in self.registro_1:
                split_linea = linea.split(',')
                qry += "(" + split_linea[0] + ","
                qry += "'" + split_linea[1] + "',"
                qry += split_linea[2] + ","
                qry += split_linea[3] + ","
                qry += split_linea[4] + ","
                qry += split_linea[5] + "),"
            qry = qry[:-1]
            cursor.execute(qry)
            cursor.close()
        return

    def insert_registro_2(self, fecha_proceso):
        """
        Inserta en BBDD los contenidos del registro 2 del archivo C47 a una fecha de proceso.
        Los contenidos deben estar almacenados en self.registro_2.

        :param fecha_proceso: la fecha de proceso del archivo.
        :return:
        """
        conn_string = "dbname=balance user=postgres password=Binter2015 host=172.16.50.40 port =5432"
        with psycopg2.connect(conn_string) as conn:
            cursor = conn.cursor()
            qry = "DELETE FROM public.c47_registro_2 WHERE fecha = '" + fecha_proceso + "'"
            cursor.execute(qry)
            qry = "INSERT INTO public.c47_registro_2 (registro, fecha, consolidacion, instrumento_captacion, "
            qry += "concentracion_instrumento, plazo_residual) VALUES "
            for linea in self.registro_2:
                split_linea = linea.split(',')
                qry += "(" + split_linea[0] + ","
                qry += "'" + split_linea[1] + "',"
                qry += split_linea[2] + ","
                qry += split_linea[3] + ","
                qry += split_linea[4] + ","
                qry += split_linea[5] + "),"
            qry = qry[:-1]
            cursor.execute(qry)
            cursor.close()
        return

    def insert_registro_3(self, fecha_proceso):
        """
        Inserta en BBDD los contenidos del registro 3 del archivo C47 a una fecha de proceso.
        Los contenidos deben estar almacenados en self.registro_3.

        :param fecha_proceso: la fecha de proceso del archivo.
        :return:
        """
        conn_string = "dbname=balance user=postgres password=Binter2015 host=172.16.50.40 port =5432"
        with psycopg2.connect(conn_string) as conn:
            cursor = conn.cursor()
            qry = "DELETE FROM public.c47_registro_3 WHERE fecha = '" + fecha_proceso + "'"
            cursor.execute(qry)
            qry = "INSERT INTO public.c47_registro_3 (registro, fecha, consolidacion, instrumento_captacion, "
            qry += "contraparte, moneda, vencimiento_contractual, captaciones, renovaciones_mayoristas_dap, "
            qry += "tasa_de_interes) VALUES "
            for linea in self.registro_3:
                split_linea = linea.split(',')
                qry += "(" + split_linea[0] + ","
                qry += "'" + split_linea[1] + "',"
                qry += split_linea[2] + ","
                qry += split_linea[3] + ","
                qry += split_linea[4] + ","
                qry += split_linea[5] + ","
                qry += split_linea[6] + ","
                qry += split_linea[7] + ","
                qry += split_linea[8] + ","
                qry += split_linea[9] + "),"
            qry = qry[:-1]
            cursor.execute(qry)
            cursor.close()
        return

    def get_registro_1(self, dia, mes, agno):
        """
        - Lee el archivo normativo C47, que se debe llamar C47DDMM.dat y debe estar en la misma ruta que esta aplicación.
        - Genera un archivo csv (separado por comas y con encabezados) con la data del registro 1.
        - Guarda la data leída en self.registro_1 para poder insertarla luego en BBDD.

        Los campos son:

        - registro
        - fecha
        - consolidacion
        - contraparte
        - concentracion
        - mayorista
        - renovacion_mayorista

        :param dia: dia de la fecha de proceso del archivo (es un padded string con 0).
        :param mes: mes de la fecha de proceso del archivo (es un padded string con 0).
        :param agno: año de la fecha de proceso del archivo (es un padded string con 0).
        :return:
        """
        archivo = './c47' + dia + mes + '.dat'
        fecha_proceso = agno + mes + dia
        fecha_proceso_1 = agno + '-' + mes + '-' + dia
        output = []
        with open(archivo, 'r') as datos:
            for fila in datos:
                if fila[0:2] == '01':
                    registro = str(int(fila[0:2]))
                    fecha = str(fila[2:10])
                    if fecha != fecha_proceso:
                        msj = "El archivo no corresponde a la fecha de proceso solicitada, " + fecha_proceso + ". "
                        msj += "Dentro del archivo la fecha de proceso es " + fecha + "."
                        raise ValueError(msj)
                    fecha = fecha_proceso_1
                    consolidacion = str(int(fila[10:11]))
                    contraparte = str(int(fila[11:13]))
                    concentracion_mayorista = str(int(fila[13:16]))
                    renovacion_mayorista = str(int(fila[16:19]))
                    output.append(registro + "," +
                                  fecha + "," +
                                  consolidacion + "," +
                                  contraparte + "," +
                                  concentracion_mayorista + "," +
                                  renovacion_mayorista)

        titulos = 'registro,fecha,consolidacion,contraparte,concentracion_mayorista,renovacion_mayorista'
        archivo = agno + mes + dia + 'C47_registro_1.csv'
        with open(archivo, 'w') as salida:
            salida.write(titulos + '\n')
            for linea in output:
                salida.write(linea + '\n')

        self.registro_1 = output

    def get_registro_2(self, dia, mes, agno):
        """
        - Lee el archivo normativo C47, que se debe llamar C47DDMM.dat y debe estar en la misma ruta que esta aplicación.
        - Luego genera un archivo csv (separado por comas y con encabezados) con la data del registro 2.
        - Guarda la data leída en self.registro_2 para poder insertarla luego en BBDD.

        Los campos son:

        - registro
        - fecha
        - consolidacion
        - instrumento_captacion
        - concentracion_instrumento
        - plazo_residual

        :param dia: dia de la fecha de proceso del archivo (es un padded string con 0).
        :param mes: mes de la fecha de proceso del archivo (es un padded string con 0).
        :param agno: año de la fecha de proceso del archivo (es un padded string con 0).
        :return:
        """
        archivo = './c47' + dia + mes + '.dat'
        fecha_proceso = agno + mes + dia
        fecha_proceso_1 = agno + '-' + mes + '-' + dia
        output = []
        with open(archivo, 'r') as datos:
            for fila in datos:
                if fila[0:2] == '02':
                    registro = str(int(fila[0:2]))
                    fecha = str(fila[2:10])
                    if fecha != fecha_proceso:
                        msj = "El archivo no corresponde a la fecha de proceso solicitada, " + fecha_proceso + ". "
                        msj += "Dentro del archivo la fecha de proceso es " + fecha + "."
                        raise ValueError(msj)
                    fecha = fecha_proceso_1
                    consolidacion = str(int(fila[10:11]))
                    instrumento_captacion = str(int(fila[11:12]))
                    concentracion_instrumento = str(int(fila[12:15]))
                    plazo_residual = str(int(fila[15:20]))
                    output.append(registro + "," +
                                  fecha + "," +
                                  consolidacion + "," +
                                  instrumento_captacion + "," +
                                  concentracion_instrumento + "," +
                                  plazo_residual)

        titulos = 'registro,fecha,consolidacion,instrumento_captacion,concentracion_instrumento,plazo_residual'
        archivo = agno + mes + dia + 'C47_registro_2.csv'
        with open(archivo, 'w') as salida:
            salida.write(titulos + '\n')
            for linea in output:
                salida.write(linea + '\n')

        self.registro_2 = output

    def get_registro_3(self, dia, mes, agno):
        """
        - Lee el archivo normativo C47, que se debe llamar C47DDMM.dat y debe estar en la misma ruta que esta aplicación.
        - Luego genera un archivo csv (separado por comas y con encabezados) con la data del registro 3.
        - Guarda la data leída en self.registro_3 para poder insertarla luego en BBDD.

        Los campos son:

        - registro
        - fecha
        - consolidacion
        - instrumento_captacion
        - contraparte
        - moneda
        - vencimiento_contractual
        - captaciones
        - renovaciones_mayoristas_dap
        - tasa_de_interes

        :param dia: dia de la fecha de proceso del archivo (es un padded string con 0).
        :param mes: mes de la fecha de proceso del archivo (es un padded string con 0).
        :param agno: año de la fecha de proceso del archivo (es un padded string con 0).
        :return:
        """
        archivo = './c47' + dia + mes + '.dat'
        fecha_proceso = agno + mes + dia
        fecha_proceso_1 = agno + '-' + mes + '-' + dia
        output = []
        with open(archivo, 'r') as datos:
            for fila in datos:
                if fila[0:2] == '03':
                    registro = str(int(fila[0:2]))
                    fecha = str(fila[2:10])
                    if fecha != fecha_proceso:
                        msj = "El archivo no corresponde a la fecha de proceso solicitada, " + fecha_proceso + ". "
                        msj += "Dentro del archivo la fecha de proceso es " + fecha + "."
                        raise ValueError(msj)
                    fecha = fecha_proceso_1
                    consolidacion = str(int(fila[10:11]))
                    instrumento_captacion = str(int(fila[11:12]))
                    contraparte = str(int(fila[12:14]))
                    moneda = str(int(fila[14:17]))
                    vencimiento_contractual = str(int(fila[17:18]))
                    captaciones = str(int(fila[18:32]))
                    renovaciones_mayoristas_dap = str(int(fila[32:46]))
                    tasa_de_interes = str(int(fila[46:60]) / 100.0)
                    output.append(registro + "," +
                                  fecha + "," +
                                  consolidacion + "," +
                                  instrumento_captacion + "," +
                                  contraparte + "," +
                                  moneda + "," +
                                  vencimiento_contractual + "," +
                                  captaciones + "," +
                                  renovaciones_mayoristas_dap + "," +
                                  tasa_de_interes)

        titulos = 'registro,fecha,consolidacion,instrumento_captacion,contraparte,moneda,vencimiento_contractual,'
        titulos += 'captaciones,renovaciones_mayoristas_dap,tasa_de_interes'
        archivo = agno + mes + dia + 'C47_registro_3.csv'
        with open(archivo, 'w') as salida:
            salida.write(titulos + '\n')
            for linea in output:
                salida.write(linea + '\n')

        self.registro_3 = output


if __name__ == "__main__":
    app = QApplication(sys.argv)
    widget = Ventana()
    widget.show()
    sys.exit(app.exec_())
