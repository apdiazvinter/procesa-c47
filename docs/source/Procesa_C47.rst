Procesa\_C47 package
====================

Submodules
----------

Procesa\_C47\.aux\_functions module
-----------------------------------

.. automodule:: aux_functions
    :members:
    :undoc-members:
    :show-inheritance:

Procesa\_C47\.procesa\_c47 module
---------------------------------

.. automodule:: procesa_c47
    :members:
    :undoc-members:
    :show-inheritance:
