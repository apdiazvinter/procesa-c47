.. Procesa C47 documentation master file, created by
   sphinx-quickstart on Fri Jul 27 12:34:44 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentación de Procesa C47
============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
