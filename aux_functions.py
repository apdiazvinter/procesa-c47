# -*- coding: utf-8 -*-


def pad_integer(n):
    """
    Función que *padea* (rellena con ceros a la izquierda en algunos casos) un número entero.

    :param n: el entero que se quiere *padear*.
    :return: si n <= 0 o se retorna n como entero. Si n > 0 y n < 10 se retorna '0n'. Si n > 9 se retorna str(n).
    """
    if n <= 0:
        return n
    if n < 10:
        return '0' + str(n)
    else:
        return str(n)
